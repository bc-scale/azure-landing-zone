#!/bin/sh

# Change the contents of this output to get the environment variables
# of interest. The output must be valid JSON, with strings for both
# keys and values.
cat <<EOF
{
  "billing_account": "$BILLING_ACCOUNT",
  "billing_profile": "$BILLING_PROFILE",
  "billing_invoice": "$BILLING_INVOICE"
}
EOF
