locals {
  hierarchy     = jsondecode(file(var.hierarchy_file))
  subscriptions = local.hierarchy.subscriptions
}

data "azurerm_billing_mca_account_scope" "billing" {
  billing_account_name = var.billing.account
  billing_profile_name = var.billing.profile
  invoice_section_name = var.billing.invoice
}
