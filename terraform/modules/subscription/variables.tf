variable "hierarchy_file" {
  type        = string
  description = "Path to the hierarchy json file used to create subscriptions."
  default     = "./schema/subscriptions.json"
}

variable "management_groups" {
  type        = map(any)
  description = "Mapping of management groups."
}

variable "billing" {
  type = object({
    account = string
    profile = string
    invoice = string
  })

  description = "Billing object used for lookup of a billing scope."
  sensitive   = true
}
