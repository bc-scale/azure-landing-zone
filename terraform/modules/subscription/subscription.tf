resource "azurerm_subscription" "subscription" {
  for_each = {
    for sub in local.subscriptions : sub.name => sub
  }

  subscription_name = each.value.name
  billing_scope_id  = data.azurerm_billing_mca_account_scope.billing.id
}

resource "azurerm_management_group_subscription_association" "association" {
  for_each = {
    for sub in local.subscriptions : sub.name => sub
  }

  management_group_id = var.management_groups[each.value.name].id
  subscription_id     = "/subscriptions/${azurerm_subscription.subscription[each.value.name].subscription_id}"
}
