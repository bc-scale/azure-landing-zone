output "management_groups" {
  description = "Map with attributes of all the management groups created."

  value = merge(
    {
      for mg in azurerm_management_group.level1 : mg.display_name => {
        "id"   = mg.id
        "name" = mg.name
      }
    },
    {
      for mg in azurerm_management_group.level2 : mg.display_name => {
        "id"   = mg.id
        "name" = mg.name
      }
    },
    {
      for mg in azurerm_management_group.level3 : mg.display_name => {
        "id"   = mg.id
        "name" = mg.name
      }
    }
  )
}
