terraform {
  required_providers {
    azapi = {
      source  = "Azure/azapi"
      version = ">=0.3.0"
    }
  }
}

locals {
  hierarchy = jsondecode(file(var.hierarchy_file))

  settings          = local.hierarchy.hierarchySettings
  management_groups = local.hierarchy.managementGroups
}

data "azurerm_management_group" "root" {
  display_name = local.management_groups[0].parent
}

data "azurerm_management_group" "default" {
  display_name = local.settings.defaultManagementGroup

  depends_on = [
    azurerm_management_group.level3
  ]
}

resource "azapi_resource" "hierarchy_settings" {
  type      = "Microsoft.Management/managementGroups/settings@2021-04-01"
  name      = "default"
  parent_id = data.azurerm_management_group.root.id

  body = jsonencode({
    properties = {
      defaultManagementGroup               = data.azurerm_management_group.default.id
      requireAuthorizationForGroupCreation = local.settings.requireAuthorizationForGroupCreation
    }
  })

  depends_on = [
    azurerm_management_group.level3
  ]
}
