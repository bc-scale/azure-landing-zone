variable "hierarchy_file" {
  type        = string
  description = "Path to the hierarchy json file used to create management groups."
  default     = "./schema/management-groups.json"
}
