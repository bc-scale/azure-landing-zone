resource "azurerm_management_group" "level1" {
  for_each = {
    for mg in local.management_groups : mg.name => mg if mg.level == 1
  }

  display_name               = each.value.name
  parent_management_group_id = data.azurerm_management_group.root.id
}

resource "azurerm_management_group" "level2" {
  for_each = {
    for mg in local.management_groups : mg.name => mg if mg.level == 2
  }

  display_name               = each.value.name
  parent_management_group_id = azurerm_management_group.level1[each.value.parent].id
}

resource "azurerm_management_group" "level3" {
  for_each = {
    for mg in local.management_groups : mg.name => mg if mg.level == 3
  }

  display_name               = each.value.name
  parent_management_group_id = azurerm_management_group.level2[each.value.parent].id
}
