terraform {
  required_version = ">= 1.2.0"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.8.0"
    }
  }
}

provider "azurerm" {
  features {}
}

locals {
  billing = {
    account = data.external.env.result["billing_account"]
    profile = data.external.env.result["billing_profile"]
    invoice = data.external.env.result["billing_invoice"]
  }
}

data "external" "env" {
  program = ["${path.module}/scripts/env.sh"]
}

module "management_group" {
  source = "./modules/management-group"
}

module "subscription" {
  source = "./modules/subscription"

  management_groups = module.management_group.management_groups
  billing           = local.billing
}
