stages:
  - lint
  - scan
  - plan
  - apply

variables:
  GITLAB_PROJECT: ${CI_API_V4_URL}/projects/$CI_PROJECT_ID
  BACKEND_ADDRESS: ${GITLAB_PROJECT}/terraform/state/$CI_PROJECT_NAME
  TF_ROOT: ${CI_PROJECT_DIR}/terraform
  TF_CACHE: ${TF_ROOT}/.terraform

cache:
  paths:
    - $TF_CACHE

.terraform_init:
  image:
    name: hashicorp/terraform:1.3.2
    entrypoint: [""]
  before_script:
    - cd $TF_ROOT
    - terraform init
        -backend-config=address=$BACKEND_ADDRESS
        -backend-config=lock_address=${BACKEND_ADDRESS}/lock
        -backend-config=unlock_address=${BACKEND_ADDRESS}/lock
        -backend-config=username=$TF_BACKEND_USER
        -backend-config=password=$TF_BACKEND_PASSWORD
        -backend-config=lock_method=POST
        -backend-config=unlock_method=DELETE
        -backend-config=retry_wait_min=5

validate-json:
  stage: lint
  image:
    name: registry.gitlab.com/massimocannavo/docker-images/jsonschema:4.15.0
    entrypoint: [""]
  script:
    - cd $TF_ROOT
    - python /home/jsonschema/json_schema.py --path schema

validate-terraform:
  stage: lint
  extends:
    - .terraform_init
  script:
    - terraform validate

scan-terraform:
  stage: scan
  image:
    name: aquasec/tfsec
    entrypoint: [""]
  script:
    - tfsec $TF_ROOT

plan-terraform:
  stage: plan
  extends:
    - .terraform_init
  script:
    - terraform plan -out terraform.tfplan
  artifacts:
    expire_in: 8 hours
    paths:
      - ${TF_ROOT}/terraform.tfplan

apply-terraform:
  stage: apply
  extends:
    - .terraform_init
  script:
    - terraform apply -auto-approve terraform.tfplan
  when: manual
  allow_failure: false
