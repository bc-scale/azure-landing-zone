# Azure Landing Zone

![Azure Landing Zone](img/landing-zone.jpg)

An [**Azure landing zone**](https://tinyurl.com/45x4whjd) is the output of a
multisubscription Azure environment that accounts for scale, security
governance, networking, and identity. An Azure landing zone enables application
migration, modernization, and innovation at enterprise-scale in Azure. This
approach considers all platform resources that are required to support the
customer's application portfolio and doesn't differentiate between
infrastructure as a service or platform as a service.

# Prerequisites

You can refer to the list of links below for more information on the
prerequisites mentioned in this guide.

- [**Enable Management Groups**](https://tinyurl.com/mrx4tkrr)
- [**Elevate Access for a Global Administrator**](https://tinyurl.com/3nzyxcd7)
- [**Create Azure AD Group**](https://tinyurl.com/ms99pz9r)
- [**Management Group Access**](https://tinyurl.com/tbmry8bf)
- [**Assign Role to Group**](https://tinyurl.com/ytsuykbw)
- [**Create Service Principal**](https://tinyurl.com/2s35wx78)
- [**Azure Built-in Roles**](https://tinyurl.com/4dyzr2cm)

A group named **azure-platform-owners** will be used to manage management
groups and subscriptions. The group must be assigned the **Owner** role at the
**tenant root (/)** scope. Deployment of Azure landing zone will be executed
using a service principal named **azure-lz-deployer**.

## Verify Service Principal Groups

In order to be able to automate the deployment of the landing zone, a service
principal will require appropriate permissions that are not too open. It's a good
idea to follow the **principle of least privilege** when granting access either
through the portal or via code.

```bash
principal_id=$(
  az ad sp list \
    --display-name azure-lz-deployer \
    --query "[].id" \
    --output tsv
)

az rest --method get \
  --url https://graph.microsoft.com/v1.0/servicePrincipals/${principal_id}/memberOf \
  --query 'value[].displayName'

[
  "azure-platform-owners",
  "Groups Administrator",
  "azure-mca-subscription-creators"
]
```

Make sure the service principal is a member of the groups above. You will need
to create two custom groups named: **azure-platform-owners** and
**azure-mca-subscription-creators**. The other group,
[**Groups Administrator**](https://tinyurl.com/yehy9mdc) is an Azure AD
built-in-role that allows managing groups in Azure AD.

## Verify Role Assignment

[**Azure role-based access control (Azure RBAC)**](https://tinyurl.com/2b9vxvw9)
has several Azure built-in roles that you can assign to users, groups, service
principals, and managed identities. Role assignments are the way you control
access to Azure resources.

```bash
management_group=$(
  az account management-group list \
    --query "[?displayName=='Tenant Root Group'].id" \
    --output tsv
)

az role assignment list \
  --scope $management_group \
  --query "[].{
    principalName:principalName,
    principalType:principalType,
    roleDefinitionName:roleDefinitionName,
    scope:scope,
    type:type
  }"

[
  {
    "principalName": "azure-platform-owners",
    "principalType": "Group",
    "roleDefinitionName": "Owner",
    "scope": "/providers/Microsoft.Management/managementGroups/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    "type": "Microsoft.Authorization/roleAssignments"
  }
]
```

A group named **azure-mca-subscription-creators** will need to be created and
assigned the
[**Azure subscription creator billing role**](https://tinyurl.com/yckz9ym7). This
group will allow the service principal to be able to create new subscriptions.

## Verify Billing Role Assignment

A [**billing account**](https://tinyurl.com/3w3dzsf9) is created when you sign
up to use Azure. You use your billing account to manage invoices, payments, and
track costs. Roles on the billing account have the highest level of permissions
and users in these roles get visibility into the cost and billing information
for your entire account.

```bash
billing_account=$(
  az billing account list \
    --query "[].name" \
    --output tsv
)

billing_profile=$(
  az billing profile list \
    --account-name $billing_account \
    --query "[].name" \
    --output tsv
)

billing_invoice=$(
  az billing invoice section list \
    --account-name $billing_account \
    --profile-name $billing_profile \
    --query "[].name" --output tsv
)

group_id=$(
  az ad group show \
    --group azure-mca-subscription-creators \
    --query id \
    --output tsv
)

az billing role-assignment list \
  --account-name $billing_account \
  --profile-name $billing_profile \
  --invoice-section-name $billing_invoice \
  --query "[?principalId=='$group_id'].{group:principalId, scope:scope, type:type}"

[
  {
    "group": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    "scope": "/providers/Microsoft.Billing/billingAccounts/xxxx/billingProfiles/xxxx/invoiceSections/xxxx",
    "type": "Microsoft.Billing/billingRoleAssignments"
  }
]
```

## Verify Billing Role Definition

A [**role definition**](https://tinyurl.com/4vx3fyhz) is a collection of
permissions. It's sometimes just called a role. A role definition lists the
actions that can be performed, such as read, write, and delete. It can also
list the actions that are excluded from allowed actions or actions related
to underlying data.

```bash
billing_role=$(
  az billing role-assignment list \
    --account-name $billing_account \
    --profile-name $billing_profile \
    --invoice-section-name $billing_invoice \
    --query "[?principalId=='$group_id'].roleDefinitionId" \
    --output tsv
)

az billing role-definition list \
  --account-name $billing_account \
  --profile-name $billing_profile \
  --invoice-section-name $billing_invoice \
  --query "[?id=='$billing_role'].{roleName:roleName, type:type}"

[
  {
    "roleName": "Azure subscription creator",
    "type": "Microsoft.Billing/billingAccounts/billingProfiles/invoiceSections/billingRoleDefinitions"
  }
]
```

# Management Groups

If your organization has many Azure subscriptions, you may need a way to
efficiently manage access, policies, and compliance for those subscriptions.
Management groups provide a governance scope above subscriptions. You organize
subscriptions into management groups the governance conditions you apply cascade
by inheritance to all associated subscriptions.

Management groups give you enterprise-grade management at scale no matter what
type of subscriptions you might have. However, all subscriptions within a single
management group must trust the same Azure Active Directory (Azure AD) tenant.

The following diagram shows the hierarchy of the management groups used.

![Management Group Hierarchy](img/management-groups.png)

# Platform Subscriptions

Subscriptions are a unit of management, billing, and scale within Azure. They
play a critical role when you're designing for large-scale Azure adoption.
Subscriptions serve as a scale unit so component workloads can scale within
platform subscription limits. Make sure you consider subscription resource
limits as you design your workloads.

Subscriptions provide a management boundary for governance and isolation that
clearly separates concerns. Create separate platform subscriptions for
management (monitoring), connectivity, and identity when they're required.

Establish a dedicated management subscription in your platform management group
to support global management capabilities like Azure Monitor Log Analytics
workspaces and Azure Automation runbooks.

Establish a dedicated identity subscription in your platform management group to
host Windows Server Active Directory domain controllers when needed.

Establish a dedicated connectivity subscription in your platform management
group to host an Azure Virtual WAN hub, private Domain Name System (DNS),
ExpressRoute circuit, and other networking resources. A dedicated subscription
ensures that all your foundation network resources are billed together and
isolated from other workloads.

![Subscriptions](img/subscriptions.png)
